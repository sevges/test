<?php

namespace App\Controller;

use App\Util\Parser;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class DefaultController
{
    public function index()
    {
        $request = Request::createFromGlobals();

        $json = $request->query->get('text');

        $text = Parser::parse($json);

        $json = Parser::encode($text);

        return new Response($json);
    }
}
