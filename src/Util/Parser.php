<?php
/**
 * Created by PhpStorm.
 * User: sergey
 * Date: 06.02.2018
 * Time: 9:17
 */

namespace App\Util;

/**
 * Class Parser
 *
 * @package App\Util
 */
class Parser
{
    /**
     * Обработка текста
     *
     * @param string $json
     *
     * @return bool
     */
    public static function parse($json)
    {
        // TODO: Реализовать проверку входных параметров

        // В прежнем значении $json уже нет необходимости
        $json = json_decode($json, true);

        if (empty($json)) {
            return false;
        }

        // TODO: Необходимо реализовать проверку значения $json
        // TODO: Реализовать проверку полученных из JSON значений
        //
        $text = $json['job']['text'];

        //
        foreach ($json['job']['methods'] as $method) {
            $text = self::$method($text);
        }

        return $text;
    }

    /**
     * Подготовка результата
     *
     * @param $text
     *
     * @return array|string
     */
    public static function encode($text)
    {
        $text = [
            'text' => $text,
        ];

        $text = json_encode($text);

        return $text;
    }

    /**
     * Очистить от тегов
     *
     * @param $text
     *
     * @return string
     */
    private static function stripTags($text)
    {
        $text = strip_tags($text);

        return $text;
    }

    /**
     * Удалить пробелы
     *
     * @param $text
     *
     * @return string
     */
    private static function removeSpaces($text)
    {
        // Можно реализовать при помощи preg_replace. В данном случае str_replace нагляднее
        $text = str_replace(' ', '', $text);

        return $text;
    }

    /**
     * Заменить все пробелы на переносы строк
     *
     * @param $text
     *
     * @return string
     */
    private static function replaceSpacesToEol($text)
    {
        $text = str_replace(' ', "\r\n", $text);

        return $text;
    }

    /**
     * Экранировать спец-символы
     *
     * @param $text
     *
     * @return string
     */
    private static function htmlspecialchars($text)
    {
        $text = htmlspecialchars($text);

        return $text;
    }

    /**
     * Удалить символы
     *
     * @param $text
     *
     * @return string
     */
    private static function removeSymbols($text)
    {
        $text = str_replace(['[', '.', ',', '/', '!', '@', '#', '$', '%', '&', '*', '(', ')', ']'], "", $text);

        return $text;
    }

    /**
     * Преобразовать в целое число (найти в тексте)
     *
     * @param $text
     *
     * @return string
     */
    private static function toNumber($text)
    {
        // Находит все цифры в строке
        $text = preg_replace("/[^0-9]/", '', $text);

        return $text;
    }
}
