<?php

namespace App\Tests\Util;

use App\Util\Parser;
use PHPUnit\Framework\TestCase;

class ParserTest extends TestCase
{
    public function test_stripTags()
    {
        $json =
            '{
                "job": {
                    "text": "Привет, мне на <a href=\"test@test.ru\">test@test.ru</a> пришло приглашение встретиться, попить кофе с <strong>10%</strong> содержанием молока за <i>$5</i>, пойдем вместе!",
                    "methods": [
                        "stripTags"
                    ]
                }
            }';

        // Для удобства тестирования, возвращается только текст
        $text = Parser::parse($json);

        $this->assertEquals('Привет, мне на test@test.ru пришло приглашение встретиться, попить кофе с 10% содержанием молока за $5, пойдем вместе!', $text);
    }

    public function test_removeSpaces()
    {
        $json =
            '{
                "job": {
                    "text": "Привет, мне на <a href=\"test@test.ru\">test@test.ru</a> пришло приглашение встретиться, попить кофе с <strong>10%</strong> содержанием молока за <i>$5</i>, пойдем вместе!",
                    "methods": [
                        "removeSpaces"
                    ]
                }
            }';

        // Для удобства тестирования, возвращается только текст
        $text = Parser::parse($json);

        $this->assertEquals('Привет,мнена<ahref="test@test.ru">test@test.ru</a>пришлоприглашениевстретиться,попитькофес<strong>10%</strong>содержаниеммолоказа<i>$5</i>,пойдемвместе!', $text);
    }

    public function test_replaceSpacesToEol()
    {
        $json =
            '{
                "job": {
                    "text": "Привет, мне на <a href=\"test@test.ru\">test@test.ru</a> пришло приглашение встретиться, попить кофе с <strong>10%</strong> содержанием молока за <i>$5</i>, пойдем вместе!",
                    "methods": [
                        "replaceSpacesToEol"
                    ]
                }
            }';

        // Для удобства тестирования, возвращается только текст
        $text = Parser::parse($json);

        $this->assertEquals("Привет,\r\nмне\r\nна\r\n<a\r\nhref=\"test@test.ru\">test@test.ru</a>\r\nпришло\r\nприглашение\r\nвстретиться,\r\nпопить\r\nкофе\r\nс\r\n<strong>10%</strong>\r\nсодержанием\r\nмолока\r\nза\r\n<i>$5</i>,\r\nпойдем\r\nвместе!", $text);
    }

    public function test_htmlspecialchars()
    {
        $json =
            '{
                "job": {
                    "text": "Привет, мне на <a href=\"test@test.ru\">test@test.ru</a> пришло приглашение встретиться, попить кофе с <strong>10%</strong> содержанием молока за <i>$5</i>, пойдем вместе!",
                    "methods": [
                        "htmlspecialchars"
                    ]
                }
            }';

        // Для удобства тестирования, возвращается только текст
        $text = Parser::parse($json);

        $this->assertEquals('Привет, мне на &lt;a href=&quot;test@test.ru&quot;&gt;test@test.ru&lt;/a&gt; пришло приглашение встретиться, попить кофе с &lt;strong&gt;10%&lt;/strong&gt; содержанием молока за &lt;i&gt;$5&lt;/i&gt;, пойдем вместе!', $text);
    }

    public function test_removeSymbols()
    {
        $json =
            '{
                "job": {
                    "text": "Привет, мне на <a href=\"test@test.ru\">test@test.ru</a> пришло приглашение встретиться, попить кофе с <strong>10%</strong> содержанием молока за <i>$5</i>, пойдем вместе!",
                    "methods": [
                        "removeSymbols"
                    ]
                }
            }';

        // Для удобства тестирования, возвращается только текст
        $text = Parser::parse($json);

        $this->assertEquals('Привет мне на <a href="testtestru">testtestru<a> пришло приглашение встретиться попить кофе с <strong>10<strong> содержанием молока за <i>5<i> пойдем вместе', $text);
    }

    public function test_toNumber()
    {
        $json =
            '{
                "job": {
                    "text": "Привет, мне на <a href=\"test@test.ru\">test@test.ru</a> пришло приглашение встретиться, попить кофе с <strong>10%</strong> содержанием молока за <i>$5</i>, пойдем вместе!",
                    "methods": [
                        "toNumber"
                    ]
                }
            }';

        // Для удобства тестирования, возвращается только текст
        $text = Parser::parse($json);

        $this->assertEquals(105, $text);
    }

    public function testMisc1()
    {
        $json =
            '{
                "job": {
                    "text": "Привет, мне на <a href=\"test@test.ru\">test@test.ru</a> пришло приглашение встретиться, попить кофе с <strong>10%</strong> содержанием молока за <i>$5</i>, пойдем вместе!",
                    "methods": [
                        "stripTags", "removeSpaces"
                    ]
                }
            }';

        // Для удобства тестирования, возвращается только текст
        $text = Parser::parse($json);

        $this->assertEquals('Привет,мненаtest@test.ruпришлоприглашениевстретиться,попитькофес10%содержаниеммолоказа$5,пойдемвместе!', $text);
    }

    public function testMisc2()
    {
        $json =
            '{
                "job": {
                    "text": "Привет, мне на <a href=\"test@test.ru\">test@test.ru</a> пришло приглашение встретиться, попить кофе с <strong>10%</strong> содержанием молока за <i>$5</i>, пойдем вместе!",
                    "methods": [
                        "stripTags", "removeSpaces", "removeSymbols"
                    ]
                }
            }';

        // Для удобства тестирования, возвращается только текст
        $text = Parser::parse($json);

        $this->assertEquals('Приветмненаtesttestruпришлоприглашениевстретитьсяпопитькофес10содержаниеммолоказа5пойдемвместе', $text);
    }
}
